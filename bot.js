import Discord from 'discord.js';
import dot from 'dot';
import numberFormatter from 'format-number';
import friendlyTime from 'friendly-time';
import fs from 'fs';
import {
  entries,
  keys,
  pick,
  truncate,
} from 'lodash';
import { EMPTY } from 'rxjs';
import {
  catchError,
  skip
} from 'rxjs/operators';
import yargs from 'yargs-parser';
import auth from './auth';
import { RequestsService } from './requests.service';

// note to self: add filtering eg !xc watch sharp blade 4; price < 2000000; itemNumber = xxxx;

const templateOptions = { ...dot.templateSettings, strip: false };
const templates = {
  title: dot.template(fs.readFileSync('./templates/title.jst'), templateOptions),
  command: dot.template(fs.readFileSync('./templates/command.jst'), templateOptions),
  channel: {
    requests: dot.template(fs.readFileSync('./templates/channel-requests.jst'), templateOptions)
  },
  request: {
    header: dot.template(fs.readFileSync('./templates/request.header.jst'), templateOptions),
    body: dot.template(fs.readFileSync('./templates/request.body.jst'), templateOptions)
  }
};

const requestsService = new RequestsService();
const client = new Discord.Client();

const toParams = (paramMap) => {
  return keys(paramMap).map(paramName => ({ paramName, param: paramMap[paramName] }));
};

const renderChannelRequest = ([channelID, channelRequests]) => {
  const requestEmbed = new Discord.RichEmbed();
  const currencyFormatter = numberFormatter({ suffix: 'z' });
  const allItems = channelRequests.reduce((results, { items }) => {
    return [...results, ...items];
  }, []);
  requestEmbed
    .setTitle(templates.title({ items: allItems, currencyFormatter }))
    .setColor('0xf5f4c6');
  channelRequests.forEach(({ request, items, userID, params: paramMap }) => {
    requestEmbed.addField(
      truncate(templates.request.header({ request, params: toParams(paramMap) }), { length: 255 }),
      truncate(templates.request.body({ userID, items, currencyFormatter, dateFormatter: friendlyTime }), { length: 1024, separator: '[' })
    );
  });

  const channel = client.channels.get(channelID);
  if (channel) {
    channel.send({ embed: requestEmbed });
  }
};

client.on('ready', function() {
  console.log(`${client.user.username} running`);
  requestsService.run().pipe(
    skip(1),
    catchError(error => {
      console.log(error);
      return EMPTY;
    })
  ).subscribe(channelRequests => entries(channelRequests).forEach(renderChannelRequest));
});

client.on('message', function(message) {
  const [command, subcommand, ...parts] = message.content.split(' ');
  const { _: itemNameParts, ...opts } = yargs(parts.join(' '), {
    number: ['refine', 'slots'],
    boolean: ['broken'],
  });
  const request = itemNameParts.join(' ');
  const params = pick(opts, 'enchant', 'category', 'rarity', 'refine', 'slots', 'broken');

  if (command === '!xc') {
    switch(subcommand) {
      case 'watch':
        requestsService.watch(message.channel.id, message.author.id, request, params);
        break;
      case 'list':
        const requests = requestsService.list(message.channel.id, message.author.id);
        const listEmbed = new Discord.RichEmbed();
        listEmbed.setDescription(templates.channel.requests({ requests, toParams }));
        message.channel.send({ embed: listEmbed });
        break;
      case 'clear':
        requestsService.clear(message.channel.id, message.author.id, request);
        break;
      case 'help':
      default:
        const helpEmbed = new Discord.RichEmbed();
        helpEmbed.setDescription(templates.command());
        message.channel.send({ embed: helpEmbed });
    }
  }
});

client.login(auth.token);