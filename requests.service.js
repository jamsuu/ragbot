import {
  concat,
  differenceBy,
  entries,
  filter as _filter,
  find,
  flatMap,
  groupBy,
  isEmpty,
  isEqual,
  isNil,
  matches,
  negate as not,
} from 'lodash';
import fetch from 'node-fetch';
import { LocalStorage } from 'node-localstorage';
import {
  EMPTY,
  from,
  interval,
  merge,
  pipe,
  Subject
} from 'rxjs';
import {
  catchError,
  concatMap,
  delay,
  distinctUntilChanged,
  filter,
  map,
  pairwise,
  publish,
  refCount,
  startWith,
  switchMap,
  tap,
  toArray
} from 'rxjs/operators';
import URI from 'urijs';
import config from './config';

const REQUEST_KEY = 'requests';
const store = new LocalStorage('./store');

export class RequestsService {
  constructor() {
    this.requestsSubject$ = new Subject();

    this._convertItem = this._convertItem.bind(this);
    this._filterProcessedItems = this._filterProcessedItems.bind(this);
    this._groupRequests = this._groupRequests.bind(this);
    this._makeRequest = this._makeRequest.bind(this);

    this.newRequests$ = this.requestsSubject$.pipe(
      this._processRequests(),
    );

    this.watcher$ = interval(config.api.refreshInterval).pipe(
      map(() => this.list()),
      startWith(this.list()),
      this._processRequests(),
      distinctUntilChanged(isEqual),
    );
  }

  list(channelID, userID) {
    const storedRequests = store.getItem(REQUEST_KEY);
    const requests = storedRequests
      ? JSON.parse(storedRequests)
      : [];

    return _filter(requests, {
      ...(!!channelID && { channelID }),
      ...(!!userID && { userID })
    });
  }

  watch(channelID, userID, request, params) {
    const convertedRequest = request.toLowerCase();
    const currentRequests = this.list();
    const matchingChannelRequest = find(currentRequests, currentRequest => {
      return currentRequest.channelID === channelID &&
        currentRequest.userID === userID &&
        currentRequest.request === convertedRequest &&
        isEqual(currentRequest.params, params)
    });
    const newChannelRequest = { channelID, userID, request: convertedRequest, params };
    const requests = !matchingChannelRequest
      ? [...currentRequests, newChannelRequest]
      : currentRequests;

    store.setItem(REQUEST_KEY, JSON.stringify(requests));
    this.requestsSubject$.next([newChannelRequest]);
  }

  run() {
    return merge(this.watcher$, this.newRequests$).pipe(
      publish(),
      refCount()
    );
  }

  clear(channelID, userID, request) {
    const requests = this.list();
    const updatedRequests = requests.filter(not(matches({ channelID, userID, ...(!!request && { request }) })));
    store.setItem(REQUEST_KEY, JSON.stringify(updatedRequests));
  }

  _processRequests() {
    return pipe(
      map(this._groupRequests),
      tap(() => console.log('PING')),
      switchMap(requests => from(entries(requests)).pipe(
        delay(config.api.refreshThrottle),
        concatMap(this._makeRequest),
        catchError(() => EMPTY),
        toArray(),
        map(requests => concat(...requests)),
      )),
      map(channelRequests => groupBy(channelRequests, channelRequest => channelRequest.channelID)),
      startWith({}),
      pairwise(),
      map(this._filterProcessedItems),
      filter(channelRequests => !isEmpty(channelRequests)),
    );
  }

  _groupRequests(channelRequests) {
    const groupedRequests = groupBy(channelRequests, channelRequest => channelRequest.request);

    return entries(groupedRequests).reduce((result, [request, channelRequest]) => {
      const channelRequests = groupBy(channelRequest, _channelRequest => _channelRequest.channelID);
      entries(channelRequests).forEach(([channel, userChannelRequests]) => {
        result[request] = {
          ...result[request],
          [channel]: userChannelRequests.map(({ userID, params }) => ({ userID, params })),
        };
      });
      return result;
    }, {});
  }

  _makeRequest([request, channelRequests]) {
    const url = URI(config.api.url).addQuery('q', request);
    const response = fetch(url.toString()).then(res => res.json());

    return from(response).pipe(
      map(items => {
        const convertedItems = items.map(this._convertItem);
        return flatMap(entries(channelRequests).map(([channelID, userRequests]) => {
          return userRequests.map(({ userID, params }) => {
            const { enchant, refine, slots, broken, category, rarity } = params;
            const filteredItems = convertedItems.filter(item => {
              const matchesEnchant = isNil(enchant) || this._getEnchant(item).toLowerCase().includes(enchant.toLowerCase());
              const matchesRefinement = isNil(refine) || item.refineLv === refine;
              const matchesSlots = isNil(slots) || this._getSlots(item) === slots;
              const matchesBroken = isNil(broken) || this._getIsBroken(item) === broken;
              const matchesCategory = isNil(category) || this._getCategory(item) === category;
              const matchesRarity = isNil(rarity) || this._getRarity(item) === rarity;

              return matchesEnchant &&
                matchesRefinement &&
                matchesSlots &&
                matchesBroken &&
                matchesCategory &&
                matchesRarity;
            });
            return { channelID, userID, request, items: filteredItems, params };
          });
        }));
      }),
    );
  }

  _getCategory({ category }) {
    if (category.includes('Weapon')) {
      return 'weapon';
    }
    if (category.includes('Card')) {
      return 'card';
    }
    if (category.includes('Blueprint')) {
      return 'blueprint';
    }
    if (category.includes('Premium')) {
      return 'premium';
    }
    if (category.includes('Equipment')) {
      return 'equipment';
    }
    return null;
  }

  _getRarity({ rarity }) {
    switch (rarity) {
      case 1: return 'white';
      case 2: return 'green';
      case 3: return 'blue';
      case 4: return 'purple';
      case 5: return 'orange';
      default: return 'white';
    }
  }

  _getIsBroken({ name }) {
    return Boolean(name.substring(name.lastIndexOf('(') + 1, name.lastIndexOf(')')));
  }

  _getSlots({ name }) {
    return Number(name.substring(name.lastIndexOf('[') + 1, name.lastIndexOf(']')));
  }

  _getEnchant({ name }) {
    return name.substring(name.toLowerCase().lastIndexOf('<') + 1, name.lastIndexOf('>'));
  }

  _filterProcessedItems([previousChannels, currentChannels]) {
    return entries(currentChannels).reduce((results, [channelID, currentRequests]) => {
      const previousRequests = previousChannels[channelID] || [];
      const unprocessedRequests = currentRequests.reduce((requestResults, currentRequest) => {
        const { request, items: currentItems, params } = currentRequest;
        const previousRequest = previousRequests.find(previousRequest => {
          return previousRequest.request === request &&
            isEqual(previousRequest.params, params);
        });
        const previousItems = (previousRequest && previousRequest.items) || [];
        const unprocessedItems = differenceBy(currentItems, previousItems, item => `${item.id}`);
        if (!previousRequest || unprocessedItems.length) {
          requestResults = [
            ...requestResults,
            {
              ...currentRequest,
              items: unprocessedItems,
            }
          ];
        }
        return requestResults;
      }, []);
      if (!previousRequests || unprocessedRequests.length) {
        results[channelID] = unprocessedRequests;
      }
      return results;
    }, {});
  }

  _convertItem(item) {
    return {
      ...item,
      link: `https://www.roguard.net/db/items/${item.itemId}`,
      snapExpiry: item.lastRecord.snapEnd && new Date(item.lastRecord.snapEnd * 1000)
    };
  }
}
